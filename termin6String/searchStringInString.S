// Loesung Termin6 Rechnerarchitektur WS2011
// Name: 	Matrikelnummer:
// Name: 	Matrikelnummer:
// Datum:

@ in R0 wird die Adresse des zu durchsuchenden Text erwartet.
@ in R1 wird die Adresse des zu suchendenn Text erwartet.
@ sollte R0 auf einen leeren String zeigen,
@ oder der gesuchte Text nicht gefunden werden,
@ so wird in R0 der Wert 0 zurueck geliefert.
@ zeigt R1 auf einen leeren String so wird in R0 der Wert 0 zurueck gegeben
 
	.file	"searchStringInString.S"
	.text
	.align	2
	.global	searchStringInString
	.type	searchStringInString, %function
	
searchStringInString:

	push {r4-r12,lr}
	
	mov r4,r0	@Adresse auf String wird in R4 gespeichert
	mov r5,r1	@Adresse auf Suchstring wird in R5 gespeichert
	mov r0,#0	@anzahl = 0
	mov r1,#0	@s2cnt = 0  WIRD DAS GEBRAUCHT?
	

	ldrb r7,[r4]		@Lade 1 Byte(1Buchstabe) von String
	ldrb r8,[r5]		@Lade 1 Byte(1Buchstabe) von SuchString
	cmp r7,#0
	beq .ende
	cmp r8,#0
	beq .ende		@ Wenn String oder Suchstring leer=0 sind, beende Programm und gib 0 zur�ck

	
	mov r9,r4
	mov r10,r5
	
.while:
	mov r5,r10		@Setze R5 wieder auf Ursprungswert


	ldrb r7,[r4],#1		@Lade 1 Byte(1Buchstabe) von String und erh�he danach auf n�chstes Byte
	ldrb r8,[r5]		@Lade 1 Byte(1Buchstabe) von SuchString
	
	cmp r7,#0		
	beq .ende		@wenn String = 0, dann ist er zu ende
	
	
	cmp r7,r8		@vergleiche Buchstaben
	bne .while		@wenn ungleich, von vorne mit n�chstem Buchstaben in String
	ldrb r8,[r5],#1
	beq .vergleiche		@wenn gleich, vergleiche mit n�chstem Buchstaben in String und Suchstring
	

	
.vergleiche:
//	mov r11,r7
//	mov r12,r8
	ldrb r7,[r4],#1		@lade n�chstes Byte von String
	ldrb r8,[r5],#1		@lade n�chstes Byte von Suchstring
	
	cmp r7,r8		@vergleiche
	beq .vergleiche		@wenn gleich, vergleiche weiter
	cmp r8,#0		@Suchstring=0?
	bne .while		@nein: Suchstring nicht vollst�ndig in String, gehe zu While-Schleife
	addeq r0,r0,#1		@ja: erh�he Anzahl um 1
	beq .while		@ja: gehe zu While-Schleife
	

//	ldrb r4,[r9,#1]		
//	mov r9,r4		@starte Text 1 Buchstabe weiter




	
.ende:				@Return R0
	
	pop {r4-r12,pc}
	
	





	
	.size	searchStringInString, .-searchStringInString

